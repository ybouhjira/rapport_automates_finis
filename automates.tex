\chapter{Automates finis}

Les automates sont des machines abstraites utilisées pour la validation des mots d'un langage régulier.

\section{Automates finis déterministes}

\subsection{Définition}

Un automate fini déterministe est la donnée d'un quintuple.

\begin{equation*}
	\mathcal{A} = (Q, q_0, F, \Sigma, \delta) 
\end{equation*}

où

\begin{itemize}
	\item $Q$ est un ensemble fini dont les  ́el ́ements sont les  ́etats de $\mathcal{A}$.
	\item $q_0 \in Q$ est un  ́etat privilégié appelé état initial.
	\item $F \subseteq Q$ désigne l'ensemble des états finals.
	\item $\Sigma$ est l'alphabet de l'automate.
	\item $\delta : Q \times \Sigma \rightarrow Q $ est la fonction de transition de $\mathcal{A}$
\end{itemize}

\subsection{Représentation}
Nous représenterons un AFD $\mathcal{A}$ de la manière suivante. Les états de $\mathcal{A}$
sont les sommets d'un graphe orienté et sont représentés par des cercles. Si 
$\delta(q, \sigma) = q'$, avec $q, q' \in Q$ et $\sigma \in \Sigma$, alors on trace un arc orienté
de q vers q' et de label $\sigma$.

\begin{equation*}
	{q \xrightarrow{\qquad \sigma \qquad} q'}
\end{equation*}

Les états finaux sont repérés grâce à un double cercle et l’état initial est
désigné par une flèche entrante sans label. Enfin, si deux lettres $\sigma$ et $\sigma'$
sont telles que $\delta(q, \sigma) = q'$ et $\delta(q, \sigma') = q'$, on s'autorise à dessiner 
un unique arc portant deux labels séparés par une virgule,

\begin{equation*}
	{q \xrightarrow{\qquad \sigma, \sigma' \qquad} q'}
\end{equation*}   


\subsection{Exemple}
L'automate $\mathcal{A} = (Q, q_0, F, \Sigma, \delta)$ où $Q = \{1, 2, 3 \}$, $q_0 = 1$, 
$F = \{1, 2\}, \Sigma = \{a, b\}$ et où la fonction de transition est donnée par

\begin{table}[H]
\centering
    \begin{tabular}{l|ll}
    $\delta$ & a & b \\ \hline
    1 & 1 & 2 \\
    2 & 1 & 3 \\
    3 & 3 & 2 \\
    \end{tabular}
\end{table}

est représenté par la figure \ref{anadf}

\begin{figure}[H]
	\centering
    \begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=2.8cm, thick]
  \node[initial,state,accepting] (1)               {$1$};
  \node[state]                   (2) [right of=1]  {$2$};
  \node[state]                   (3) [right of=2] {$3$};
  
  \path [->] (1)  edge [loop above] node {a} (1);
  \path [->] (1)  edge              node {b} (2);
  \path [->] (2)  edge [bend left]  node {a} (1);
  \path [->] (2)  edge              node {b} (3);
  \path [->] (3)  edge [bend left]  node {b} (2);
  \path [->] (3)  edge [loop above] node {a} (3);
\end{tikzpicture}
    \caption{\label{anadf} Un AFD}
\end{figure}

\section{Automates finis non-détérministes}

\subsection{Définiton}
Un automate fini non déterministe est la donnée d'un quintuple.

\begin{equation*}
	\mathcal{A} = (Q, I, F, \Sigma, \Delta) 
\end{equation*}

où 
\begin{itemize}
	\item $Q$ est un ensemble fini dont les éléments sont les états de $\mathcal{A}$.
	\item $I \subseteq Q$ est l'ensemble des états initiaux.
	\item $F \subseteq Q$ désigne l'ensemble des états finals.
	\item $\Sigma$ est l'alphabet de l'automate.
	\item $\Delta \subset Q \times \Sigma^{*} \times Q$ est une relation de transition (qu'on supposera finie).
\end{itemize}


On peut dés à présent noter plusieurs différences entre
les AFD et AFND. Dans le cas non déterministe, il est possible de d'avoir \emph{plus d'un état initial}; les labels des arcs ne sont plus nécesserement des lettres 
mais bien des mots de $\Sigma^{*}$ et enfin, on n'a plus 
une fonction de transition mais une \underline{relation} 
de transition. Pour représenter les AFND, nous utilisons
les mêmes conventions que pour les AFD.

\subsection{Exemple}
L'automate de la figure \ref{unafnd}, est un AFND ayant
1 et 3 comme états initiaux, 2 comme état final et la relation et la
relation de transition est 

\begin{equation*}
	\Delta = \{(1, a, 1), (1, a, 3), (1, ba, 2), (2, a, 1), (2, b, 3), (3, b, 2)\}
\end{equation*}

\begin{figure}[H]
	\centering
    \begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=2.8cm, thick, initial text=]
  \node[initial, state]    (1)              {$1$};
  \node[state, accepting] (2) [right of=1] {$2$};
  \node[state, initial right, ]   (3) [right of=2] {$3$};
  
  \path [->] (1)  edge [loop above] node {a} (1);
  \path [->] (1)  edge              node {ba} (2);
  \path [->] (2)  edge [bend left]  node {a} (1);
  \path [->] (2)  edge              node {b} (3);
  \path [->] (3)  edge [bend left]  node {b} (2);
  \path [->] (1)  edge [bend left] node {a} (3);
\end{tikzpicture}
    \caption{\label{unafnd} Un AFND}
\end{figure}

\section{Déterminisation d'un automate}

\subsection{Théorème}

Pour tout automate fini non-déterministe $\mathcal{A}$, il existe un automate
fini déterministe $\mathcal{A}'$ reconnaissant le même langage. 

\subsection{Méthode de conversion}
La méthode utilisé pour convertir un AFND à un AFD s'appelle \emph{la construction par sous-ensembles}.

Soit l'AFND représenté par la figure \ref{afnd_constr_sous_ensembles}, en construit le tableau
suivant de proche en proche en initialisant avec $I.\varepsilon$ qui est ici ${1}.\varepsilon = {1}$

\begin{figure}[H]
	\centering
    \begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=4cm, thick, initial text=]
  \node[initial right, state]    (1)              {$1$};
  \node[state, accepting] (2) [left of=1] {$2$};
  \node[state ]   (3) [above right of=2] {$3$};
  
  \path [->] (1)  edge [loop above] node {a} (1);
  \path [->] (1)  edge              node {a} (2);
  \path [->] (1)  edge              node {a} (3);
  
  \path [->] (2)  edge [loop left]  node {b} (2);
  
  \path [->] (3)  edge           node {$\varepsilon$} (2);
  \path [->] (3)  edge [loop above]  node {c} (3);
\end{tikzpicture}
    \caption{\label{afnd_constr_sous_ensembles} Un AFND}
\end{figure}

A chaque étape, pour un sous ensemble $X$ d'états non encore traité, on détermine les valeurs de $X.\sigma$
pour tout $\sigma \in \Sigma$. La construction se termine une fois que tous les sous-ensembles d'états apparaissent
ont été pris en compte.

\begin{table}[H]
\centering
    \begin{tabular}{c||c|c|c}
    $\{X\}$        & $X.a$         & $X.b$       &$X.c$          \\ \hline
    $\{1\}$        & $\{1, 2, 3\}$ & $\emptyset$ & $\emptyset$   \\
    $\{1, 2, 3\}$  & $\{1, 2, 3\}$ & $\{2\}$     & $\{2, 3\}$    \\
    $\emptyset$    & $\emptyset$   & $\emptyset$ & $\emptyset$   \\
    $\{2\}$        & $\emptyset$   & $\{2\}$     & $\emptyset$   \\
    $\{2, 3\}$     & $\emptyset$   & $\{2\}$     & $\{2, 3\}$
    \end{tabular}
\end{table}

Si on pose $A = \{1\}$, $B = \{1, 2, 3\}$, $C = \emptyset$, $D = \{2\}$ et $E = {2, 3}$, alors on a l'automate représenté par la figure \ref{afnd_to_afd}. 

\begin{figure}[H]
	\centering
    \begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=3cm, thick, initial text=]
  \node[initial left, state] (A)                    {$A$};
  \node[state, accepting]    (B) [right of=A]        {$B$};
  \node[state ]              (C) [above of=A] {$C$};
  \node[state, accepting]    (D) [right of=C] {$D$};
  \node[state, accepting]    (E) [above right of=D] {$E$};
  
  \path [->] (A)  edge [bend right]   node {$a$}    (B);
  \path [->] (A)  edge [bend left]   node {$b, c$} (C);

  \path [->] (B)  edge [loop below]  node {$a$}    (B);  
  \path [->] (B)  edge [bend right=30]  node {$c$}    (E);
  \path [->] (B)  edge               node {$b$}    (D);
  
  \path [->] (C)  edge [loop left]  node {$a, b, c$} (C);
  
  \path [->] (D)  edge [loop above]  node {$b$} (D);
  \path [->] (D)  edge               node {$a, c$} (C);
  
  \path [->] (E)  edge [loop right]  node {$c$} (E);
  \path [->] (E)  edge               node {$b$} (D);
  \path [->] (E)  edge [bend right=45]   node {$a$} (C); 
       
  \end{tikzpicture}
    \caption{\label{afnd_to_afd} Résultat de la déterminasation}
\end{figure}

\section{Minimisation d'un automate fini}

\subsection{Méthode}
L'algorithme de minimisation du nombre d'états d'un AFD fonctionne en déterminant tous les groupes d'états qui peuvent être distingués par une chaîne d'entrée. Chaque groupe d'états indistinguables est alors fusionné en un état unique. L'algorithme travaille en mémorisant et en raffinant une partition de l'ensemble des états. Chaque groupe d'états à l'intérieur de la partition correspond aux états qui n'ont pas encore été distingués les uns des autres. Toute paire d'états extraits de différents groupes a été prouvée "distinguable" par une chaîne.

Initialement, la partition consiste à former deux groupes : les états d'acceptation et les autres. L'étape fondamentale prend un groupe d'états et un symbole puis étudie les transitions de ces états sur ce symbole. Si ces transitions conduisent à des états qui tombent dans au moins deux groupes différents de la partition courante, alors on doit diviser ce groupe. La division est effectuée avec pour objectif que les transitions depuis chaque sous-groupe soient confinées à un seul groupe de la partition courante. Ce processus de division est répété jusqu'à ce qu'aucun groupe n'ait plus besoin d'être divisé.

Les états du nouvel automate sont donnés par un représentant de chaque groupe. Les états finaux sont les représentants des groupes possédant un état final de l'AFD de départ. L'état de départ est le représentant du groupe possédant l'état de départ de l'AFD initial. Les transitions sont construites avec les transitions de chaque état représentant de groupe vers un état représentant d'un autre groupe si cet état engendre une transition vers un élément du groupe.

Sur les états créés, il faut supprimer les états stériles et les états non-accessibles depuis l'état initial.

\subsection{Exemple}

On considère le AFD représenté par la figure \ref{afd_minim} : 

\begin{figure}[H]
	\centering
    \begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=3cm, thick, initial text=]
  \node[initial left, state] (A)              {$A$};
  \node[state, accepting]     (B) [right of=A] {$B$};
  \node[state ]               (C) [above of=B] {$C$};
  \node[state , accepting]    (D) [right of=D, below of=C] {$D$};
  
  \path [->] (A)  edge              node {$a$} (B);
  \path [->] (A)  edge              node {$0$} (C);
  
  \path [->] (B)  edge [bend left]  node {$0$} (D);
  \path [->] (B)  edge [bend right]  node {$a$} (D);
    
  \path [->] (D)  edge [loop right] node {$a, b$} (D);
  
  \path [->] (C)  edge [loop above]  node {$a, 0$} (C);
\end{tikzpicture}
    \caption{\label{afd_minim} Un automate fini déterministe}
\end{figure}

Premièrement on divise les états sur deux ensembles (Acceptants et les autres) :
\begin{equation*}
	I_1 = \{A, C\}
\end{equation*}
\begin{equation*}
	I_2 = \{B, D\}
\end{equation*}

L'état $A$ mène vers $B$ qui n'appartient pas à $I_1$, donc on devise $I_1$ en 
deux ensembles
	
\begin{equation*}
	I_1 = \{A\}
\end{equation*}
\begin{equation*}
	I_2 = \{B, D\}
\end{equation*}
\begin{equation*}
	I_3 = \{C\}
\end{equation*}

Finalement on à des ensembles d'états qui se comportent tous de la même façon. Donc 
on peut procéder à la création de l'automate à partir de ces ensembles.

\begin{figure}[H]
	\centering
    \begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=3cm, thick, initial text=]
  \node[initial left, state]  (I1)              {$I_1$};
  \node[state, accepting]     (I2) [right of=A] {$I_2$};
  \node[state ]               (I3) [above of=B] {$I_3$};
  
  \path [->] (I1)  edge             node {$a$} (I2);
  \path [->] (I1)  edge              node {$0$} (I3);
  
  \path [->] (I2)  edge [loop right]  node {$a, 0$} (I2);
    
  \path [->] (I3)  edge [loop above] node {$a, 0$} (I3);
  
\end{tikzpicture}
    \caption{Automate résultat}
\end{figure}

Cependant, l'algorithme n'est pas encore terminé ! Pour l'instant, nous avons "fusionné" les états indistinguables. Il faut maintenant supprimer tous les états inutiles restant. Dans notre exemple, l'état III est un état stérile, donc inutile. Il faut donc le supprimer. D'où l'automate déterministe minimal de la figure suivante.

\begin{figure}[H]
	\centering
    \begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=5cm, thick, initial text=]
  \node[initial left, state]  (I1)              {$I_1$};
  \node[state, accepting]     (I2) [right of=A] {$I_2$};
  
  \path [->] (I1)  edge             node {$a$} (I2);
  
  \path [->] (I2)  edge [loop right]  node {$a, 0$} (I2);
  
\end{tikzpicture}
    \caption{Automate résultat}
\end{figure}
